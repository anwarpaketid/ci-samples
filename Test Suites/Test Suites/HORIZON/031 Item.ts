<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>031 Item</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>eb5e7afe-bd77-40bc-b4a0-d8de9086d003</testSuiteGuid>
   <testCaseLink>
      <guid>66b7138c-f216-4af6-b012-44e3eaa98a35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0eced72c-01f2-4e98-bdfa-560e18f70157</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/31 ITEM/IT01TC01</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8393d3e8-0cb1-4c10-8ac4-a3f73e9cfcbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4267e090-98e3-419a-bc0e-92a93d452051</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/itemId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e35e0762-3038-4240-a951-7450b895cef6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/31 ITEM/IT03TC03 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3544a27b-8e63-4313-b804-f9282234b297</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/31 ITEM/IT04TC04 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
