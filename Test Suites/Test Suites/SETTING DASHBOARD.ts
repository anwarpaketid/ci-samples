<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SETTING DASHBOARD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a7601c29-73b5-4de1-a1b1-56d0f28973d4</testSuiteGuid>
   <testCaseLink>
      <guid>e588644f-c0e4-472a-8639-f37dce49b817</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c4e6624-3b82-4c61-a3b1-9d85f02bc25d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87481374-7e04-4882-83fc-b73c0ec6339b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/stdbid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46ec5fd4-b13e-4be0-9c2f-ac4f5aebcc4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>244a41ff-e1b1-415a-8913-54f830e724d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce5a5907-686d-441c-8bff-38ce701491bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB04TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d43c49ae-b18f-4e0f-8564-07e7dd2af877</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>942f4e5f-24dd-4ce3-a7f8-d5b957eb1488</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4dc15ed7-d592-407e-9e4d-758a79f289c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
