<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>003 Location</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>eff72fa8-2400-46d2-8b84-c2cb72f8d058</testSuiteGuid>
   <testCaseLink>
      <guid>b777e31e-6692-4038-b721-022c42a783a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7451ebbd-fc79-4773-aa63-0a20a99ce3ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>190df749-3cca-4734-b5e7-a1c279c15623</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03 SETTING_LOCATION/LOC02 -- GET/LOC02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a50973e-8d40-4f08-ae30-a17823984eb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03 SETTING_LOCATION/LOC01 -- POST/LOC01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da415e07-26a8-43f2-9c5d-fd8687588aff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03 SETTING_LOCATION/LOC00 -- LocId hapus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7777dd64-3587-445b-84d7-b2c307b82ee6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eab4f83b-2e50-473a-a221-f1225cb2e795</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03 SETTING_LOCATION/LOC03 -- PUT/LOC03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>412c6410-5237-4742-89b9-462da687caf6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/03 SETTING_LOCATION/LOC04 -- DELETE/LOC04TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8244081f-3a16-4123-8a5c-fe51c439e10e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03 SETTING_LOCATION/LOC05 -- SHOW/LOC05TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
