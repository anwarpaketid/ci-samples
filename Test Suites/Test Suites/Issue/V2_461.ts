<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>V2_461</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>205663f1-cdf7-4622-941c-3d8874cdcc15</testSuiteGuid>
   <testCaseLink>
      <guid>f3e338b8-07af-4deb-a27e-1b6debe5ac5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/000 - API KEY</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff522de9-2596-4691-b0a4-d2295a596a48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/000 - LocationV2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0907461-4e0d-4e5e-91a7-cd9e346a8457</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN05 - GET Shipment Mobile/POS001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>520ae905-3be7-4c56-baf4-3ea94a35258c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/000 - LocationMile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>940847f6-fe02-421b-99ab-87d4537160de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN05 - GET Shipment Mobile/POS001</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
