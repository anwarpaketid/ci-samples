<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Issue Item Denominator</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a28ddbaf-4217-421b-a51b-6e7f9d77ed0a</testSuiteGuid>
   <testCaseLink>
      <guid>588b15da-d0e4-4602-adaf-e5c228a99edd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/ISSUE denominator/TC01 POST dengan denominator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b03d749e-fa39-4816-9d76-9cc9c7e1cb1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/ISSUE denominator/TC02 POST dengan denominator kosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47e26e57-9923-4e8b-8f53-28d8f167d5f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/ISSUE denominator/TC03 POST tanpa denominator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fce29373-12a6-4ce0-8fbf-092af3c1394d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/ISSUE denominator/TC04 PUT dengan denominator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b13cc8d-4096-4807-a303-3de14f0ebc5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/ISSUE denominator/TC05 PUT dengan denominator kosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2225b68-68cc-44fd-9db4-daa81de9b175</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/ISSUE denominator/TC06 PUT tanpa denominator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a474e680-7c36-4dde-8db9-5cd035e54ee8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/ISSUE denominator/TC07 GET data cargo item</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
