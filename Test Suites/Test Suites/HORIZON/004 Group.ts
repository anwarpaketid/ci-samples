<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>004 Group</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c091593e-3ee3-488b-a9e0-889a376cad91</testSuiteGuid>
   <testCaseLink>
      <guid>e8a97911-e126-4db4-8a29-7975cc2f5e4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c56a3f20-71d6-4dc1-a5ba-5e90f8cb049e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa947fc9-1ffd-42f4-9952-14705c8663d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b0df627-9893-48a5-808e-f5a60ed958d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a3968fd-cb96-4122-bb0e-635cca12e444</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04 GROUP (TEAM)/GRP01_GETGROUP/GRP01TC01_success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f30a69c7-ece3-494d-997d-2d8c8bf28f71</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04 GROUP (TEAM)/GRP02_CREATEGROUP/GRP02TC01_success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b99c4e44-b349-4012-b8b4-282175db936f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04 GROUP (TEAM)/GRP03_UPDATEGROUP/GRP03TC01_success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4191cec4-21b1-422c-b66c-d44cc1f60e51</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>027cbbb6-3c1b-4b5d-8a14-46bc34e43d43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/04 GROUP (TEAM)/GRP05_GRP_ALLUSER/GRP05TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5926cf07-26b8-4d8d-8bc8-83d527cb2dee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/04 GROUP (TEAM)/GRP06_GRP_MODEL/GRP06TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a75f952-d747-437c-937b-2ea4b41d9992</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/04 GROUP (TEAM)/GRP07_GRP_SUMMARY/GRP07TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
