<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>001 AUTHENTICATION</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>dd0eeffd-1ba7-4965-80df-fafa393119c5</testSuiteGuid>
   <testCaseLink>
      <guid>00d6d033-59db-44c9-9cd3-33caa0668baa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>385009c9-7791-4458-a48f-d2f712e85de4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 AUTHENTICATION/AUTH02_ORG_Login/TC_ORG_Login_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6547328-16d7-4a14-b676-ea6200344dbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 AUTHENTICATION/AUTH03_ORG_Registration/AUTH03TC01_ORG_Regis_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fef64bf9-91be-409c-978a-9fa0e0816489</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 AUTHENTICATION/AUTH04_USER_Login/AUTH04TC01_USER_Login_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae2f1cdb-64ac-4ac8-9690-e8985630597b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 AUTHENTICATION/AUTH05_FORG_Pass/AUTH05TC01_FORG_Pass_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a43928bf-5a46-4cde-a71e-255ab4483ace</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 AUTHENTICATION/AUTH06_TC_LOG/TC_LOG_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cb26f15-f5f7-4c80-9e3e-8025d05d2621</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 AUTHENTICATION/AUTH08_ORG_REQUEST/AUTH08TC01_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
