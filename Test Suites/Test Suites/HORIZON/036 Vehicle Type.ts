<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>036 Vehicle Type</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7abc01fc-c3e7-4e24-9b23-a5c720328baa</testSuiteGuid>
   <testCaseLink>
      <guid>89cd6960-5982-4f2c-b565-826d5ea15775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0552439-081c-428a-83be-7bddb6f2b6e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94a6b1c0-5423-4d64-9ada-1970e074235a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e264afc3-777c-4bae-a018-13397dcc5946</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY02 -- POST/VHTY02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6d457e0-3dd5-4719-be92-119fedaf7061</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY01 -- GET/VHTY01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>300f0d9b-8a85-4dc6-940f-f87510f60e12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/vhtype_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20ed90b8-9db6-42c5-9d99-5092b5e7e125</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY03 -- PUT/VHTY03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78e9c0e5-58a6-4fc7-8dc5-4e3f7d27d715</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY04 -- DELETE/VHTY04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
