<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>012 Task Schedule</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c93e07ec-2194-49a7-82dc-3eedc354a794</testSuiteGuid>
   <testCaseLink>
      <guid>838255d0-5cda-4e40-b57a-b7fcbc4d3a40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15e54e41-cf0e-4ab9-9fdb-1d2bd6d6031f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/getdate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14331ef2-0eff-4707-a6ea-3cf288d40613</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>442201f5-63d3-427c-af3e-527c5a7237e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f1af1c3-5f00-49de-b55a-c19eabcac187</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f461ba3-709b-4f1c-bced-48d2ba187ce3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH01 -- GET/TSCH01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33dc8395-4895-4639-9d83-bf616f708078</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e44f37dd-59c1-443d-be4c-f5819de0f7f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/taskScheduleId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88a92b4e-e7e9-4215-8b05-f5c6e61c49c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
