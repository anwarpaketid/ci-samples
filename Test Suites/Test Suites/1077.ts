<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1077</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>03c749f6-e6ac-4e98-a126-81f8317d6c0b</testSuiteGuid>
   <testCaseLink>
      <guid>768896da-6078-4a03-8ada-55285ff1c744</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00 PUBLIC/006 CARGOITEM/001 POST</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb06a6a7-dbeb-4864-a8ec-ff03b4e81057</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00 PUBLIC/006 CARGOITEM/002 GET with ID</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2bb7a5e4-7133-49a2-8ea4-e38eafe40992</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00 PUBLIC/006 CARGOITEM/003 PUT</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>478bd051-a3ea-4cda-96a7-9dd2c30ad876</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00 PUBLIC/006 CARGOITEM/002 GET with ID</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41f32aa5-e3c4-426b-b718-08b86ada2b93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00 PUBLIC/006 CARGOITEM/004 DELETE</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
