<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WMS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6a319751-d759-470e-a998-42aa774a2bba</testSuiteGuid>
   <testCaseLink>
      <guid>e2948846-8cc6-4f66-b121-b63064b39fb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>534395bf-7f9e-4509-a0d4-532a65c5325b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/47 WMS ITEM/WITM01 -- GET/WITM01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>302ad599-e688-4251-9722-d80cbe638b0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/47 WMS ITEM/WITM02 -- POST/WITM02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b6930fe-0d8f-4fdf-8be4-0487f0f643ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/47 WMS ITEM/WITM03 -- DELETE/WITM03TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
