<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>011 WMS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b2e3761c-2464-44ea-b333-62f271100925</testSuiteGuid>
   <testCaseLink>
      <guid>a7d0a05d-ba4f-4b8e-a6e4-c82a17eb49ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Api-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2036c87d-e9e6-4b6c-9004-f44a9521eb4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>faddff41-3c64-49c2-8a72-a6fd066bb41b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9369288-1085-43f3-b061-f33351d5a1bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11 WMS/WMS01 -- POST/WMS01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>431f8405-47e0-42e6-82c2-ab6f3f533c4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/wmsid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ceaa489e-4c3f-426f-a741-e1c1688ea669</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11 WMS/WMS02 -- GET/WMS02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bd56638-b25a-4f4c-b924-974c4ab0855c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11 WMS/WMS03 -- PUT/WMS03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ad63c36-b2d2-4671-96a8-ae10f46e0591</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11 WMS/WMS04 -- DELETE/WMS04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
