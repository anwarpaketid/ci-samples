<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>037 Account</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>900665ea-41fc-4f25-9f24-fc599f31925a</testSuiteGuid>
   <testCaseLink>
      <guid>3a0cc537-d067-4e6a-9b13-36d3c46ff449</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>854876f1-704e-4075-8f6e-445cc9a4629d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cefba615-8430-44f7-81f5-8a288bd8aa20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/user_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28d3f1d4-1cde-4542-a9f6-6bb92decde99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/37 ACCOUNT/ACC01 -- GENERATE KEY/ACC01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b62ceabc-d21c-4727-90e3-e215d4875898</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/37 ACCOUNT/ACC02 -- LIST KEY/ACC02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa711a6d-bb72-45a3-9e88-530613aaf040</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/37 ACCOUNT/ACC03 -- PASSWORD/ACCO3TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
