<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Task Schedule</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0a4c5fbe-0e70-43e8-b015-9dbe5de5e1aa</testSuiteGuid>
   <testCaseLink>
      <guid>209bebe7-b713-48d0-b4c2-0af5f5b356a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC08 -- wrong task_schedule id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60f4d637-64da-4090-ac42-86c7ea81e5d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC07 -- taskTitle Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d1a088f-c8c5-454a-b84d-163dad904d2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC06 -- FlowNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bf89f3a-aa42-4d08-aa30-8b031ea5b1f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC05 -- wrong auth</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a677f64e-84fa-4d06-906d-5f3e9cdf95b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC04 -- wrong url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eab1741f-9c54-4b91-85a4-f1df16a71b41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC03 -- wrong task_type id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>870773b8-573a-49e7-a7ad-4f15761ec7e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC02 -- wrong locationId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e76ec07-f090-4a29-8afe-fae1206dc45d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH03 -- PUT/TSCH03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9cba4a3-15f5-47d8-9fb9-cdb9ca26c73e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC07 -- taskTitle Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04a3cf85-276f-4d22-b86a-e6eb056f0b67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC06 -- FlowNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f58108b4-91e6-4c4f-84ad-754893dd3daf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC05 -- wrong auth</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f308825-ef12-4a18-95f7-4b036d80223b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC04 -- wrong url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>174513c8-c882-4ade-9762-f857d37d7b1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC03 -- wrong task_type id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1346b980-a4a7-4803-b49c-2ef22713e425</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC02 -- wrong locationId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebf66a59-fc78-4562-be30-429e314cd4fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH02 -- POST/TSCH02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e1697df-9db8-47c3-b03b-32db6bc21b00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH01 -- GET/TSCH01TC03 -- wrong url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6b5d8d4-8bb8-48ca-bdfa-019d8da4b923</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH01 -- GET/TSCH01TC02 -- wrongAuth key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0c8bc8a-d2fb-4710-9c1c-be13311188a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/12 TASK SCHEDULE/TSCH01 -- GET/TSCH01TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
