<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Issue Inventory Sosro</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7dfd3aa0-316e-4c40-b1d6-69994d77b05f</testSuiteGuid>
   <testCaseLink>
      <guid>ba4428f9-9b0e-4c58-bf4c-c76f96ff241b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC01 - POST Inventory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b950aa4-302c-4da7-b034-4856efc7b85e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC02 GET Inventory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3992e8f7-0822-47f5-ac90-8b72fcc6ced8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC03 GET Inventory Invalid Code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a1233a7-b2fb-43ca-87d7-14991aadf782</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC04 GET Inventory Invalid Rack</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5e90ecd-37a0-478b-988e-d1e6025fadc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC05 GET Inventory Invalid Location</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14c4cac1-e7ce-4f66-a1b7-634f85af8a99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC06 PUT Inventory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c87feff-6112-48c1-b8db-529b4bae47ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC07 PUT Inventory Invalid Code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdf9bedd-8b08-4bea-bee5-ac1256eec0ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC08 PUT Inventory Invalid Rack</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d2bd96e-a3ee-49d9-a542-f7a73ffd6ff6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC09 PUT Inventory Invalid Location</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1f6f4db-13e0-4979-8dba-153b46a8c44e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC10 PUT Inventory Code Empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>255627b0-bcbe-43ca-be66-73cd42a2b66d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC11 PUT Inventory Rack Empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8d95db2-d264-49a2-a2f4-09f06ff3dc60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC12 PUT Inventory Location Empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2b88820-5588-40ae-b6be-b0789d3f02fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Inventory Sosro/TC13 PUT Inventory All Empty</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
