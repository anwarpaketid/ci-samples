<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>048 RESIN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ef8227e6-9bc9-4aad-b38d-ea48169edc25</testSuiteGuid>
   <testCaseLink>
      <guid>c45e930f-fc5f-411c-9cc9-7f75c681ea5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Api-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a83348c7-f0d3-4f33-8a5b-428c74d684c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb82fd81-d7d5-42f3-b870-eded5a9fd903</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC02 -- NullOrg</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a74fee62-98d4-4266-80a3-f822397d8b3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC03 -- NullLocationExpected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>835ddbf6-2dca-4d03-ae89-72990b5ef96c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC04 -- NullRadius</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dec74ba-9b8c-43ad-9da9-6fa13ba60b89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC05 -- NullCourier</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a874d867-39de-4983-ae64-6f9534309133</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC06 -- OrgNameNotFound</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bce6ef95-7602-43fb-ad48-68a443085b6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC07 -- LocationExpectedOutsideCourierRadius</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbdbda97-6e44-4ca4-81f4-e67ebb8f4ad0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC08 -- RadiusNotNumber</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3854223-5667-427b-9c20-266295df8b50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN01 -- Nearest Courier/RSN01TC09 -- CourierNotNumber</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
