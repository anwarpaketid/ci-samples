<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>030 Cargo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3bbf4a37-dd7a-4d04-8873-30a765c58b8d</testSuiteGuid>
   <testCaseLink>
      <guid>d1a56f52-c047-4940-9fc6-ac52c298c6cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a868d133-5ab4-4fa3-b041-a1f04e1cecb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>855655b2-2a8f-4131-8af4-980b2471cd53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11aabc96-2a5e-46db-af46-12558b4635fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/30 -- CARGO/CARGO -- CREATE/CI03</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cc108c8-0914-4986-aadd-f51e95c68a27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/ciid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04f8ed3f-973f-4b39-9291-9ade8e6a8241</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/30 -- CARGO/CARGO -- GET/CARGO -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed71bc03-14cb-4c9e-9f95-a88078dd389c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/30 -- CARGO/CARGO -- UPDATE/CI13 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
