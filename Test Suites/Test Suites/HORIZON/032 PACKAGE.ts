<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>032 PACKAGE</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>42723e9b-447e-41e1-9164-ab563474ab3a</testSuiteGuid>
   <testCaseLink>
      <guid>bedb3628-48eb-4f75-a2de-5f6282dac102</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f7b17a5-92dc-4155-8d7a-5f2e5a0238c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/pckgId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a470ccf2-a654-40c3-80c3-d6361cee49dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/32 PACKAGE/Get Package-list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08c275e3-c123-457e-8771-85c647b8271e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/32 PACKAGE/PCKG01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82815751-e10a-4961-aa32-c3a28a000518</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/32 PACKAGE/PCKG01TC02 -- PackageListSuccess</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3f84e2b-feaa-433e-855b-f519038dece5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/32 PACKAGE/PCKG01TC03 -- SuccessById</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cf022dd-a908-44f0-98b4-50c861b6c924</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/32 PACKAGE/PCKG03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>506f3bb1-f17f-4a84-a1ce-52467d6adcfd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/32 PACKAGE/PCKG04TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fdff039-a634-4a06-8976-684782115318</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/32 PACKAGE/PCKG05TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
