<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Issue Move</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>88448104-0a44-4545-891c-cf85b4da3e8a</testSuiteGuid>
   <testCaseLink>
      <guid>dc6e782c-b551-45ff-9586-22aa9bd555fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Api-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33ec074e-6181-4c35-96df-ddf8e0e13980</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Move/TC01 Validate Move</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c296cd9e-5b9b-4cb7-a2b1-e7386429a72f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Move/TC02 Mobile Process</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22316538-ab90-404c-bf37-2f0bee890864</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue Move/TC03 Validate Move (Jumlah Yg dipindahkan lebih besar dari Qty)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
