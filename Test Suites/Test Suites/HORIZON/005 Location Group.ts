<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>005 Location Group</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d80af2e0-c1b8-48ea-a634-72ea7b281a8d</testSuiteGuid>
   <testCaseLink>
      <guid>45e3dcf1-a6d5-4903-8839-2235120ab0ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0c34a7d-02dc-4bde-bf36-c0bf4b3fc3fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>beac359f-b959-42e4-8e86-6743f62f71a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/LCGP_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cede3516-40e3-4698-bd11-c8c38d79147a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/orgid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7fea7a28-acc5-4f5c-bc2b-93deb00d5df9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5bacb75-42a9-43fe-9391-4073f215b2d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05 LOCGROUP/LCGP01_LCG_GET/LCGP01TC01_LCG_GET_Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4badd298-ba55-4867-a8ff-04f2652769fe</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e1c8f5ad-66f2-4f8e-9760-e89ef94e116d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05 LOCGROUP/LCGP02_LCG_CREATE/LCGP02TC01_LCG_CREATE_Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1af74c68-2d2f-41b0-9a92-f8b37e34967b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8034cb79-4e40-4f07-b888-b5d7440614d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05 LOCGROUP/LCGP03_LCG_UPDATE/LCGP03TC01_LCG_UPDATE_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
