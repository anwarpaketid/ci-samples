<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>PUT Cargo 01</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8afc939e-a975-4a0f-b7c3-cbd26322feaa</testSuiteGuid>
   <testCaseLink>
      <guid>9d162ddf-a689-4f6f-acad-6fce444ca355</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85c8007f-c9b4-45be-8d53-167bac0529d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0bd093f-e832-4a2c-a8cd-b79692be83c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>735f82a6-6186-4cc6-a800-71c715ed0195</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Positive/TCPOSITIVE002 - Format Datetime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d93f3faa-f035-4831-9fdd-1515da0c9f98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCPOSITIVE001 - PUT Task Item Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6d93e48-5c75-43c5-a2d0-3b25abd3a324</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCPOSITIVE002 - PUT Edit Task Item</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e671d61e-f4be-45ce-a55c-5f3a3d58dbfc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCPOSITIVE003 - PUT Delete Task Item</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcecd779-63b8-4e32-820a-9ab1668e0fc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCNEGATIVE001 - PUT Task Item Baru QtyStringkosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46972130-b052-499c-a2b1-2a75abcfc12a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCNEGATIVE002 - PUT Delete tanpa mengosongkan UserVar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
