<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>025 Setting Permission</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ff2c708a-b398-4f31-831b-3d6b73dd7750</testSuiteGuid>
   <testCaseLink>
      <guid>4e861702-3823-4857-95db-db71e94b2958</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3fce0166-6bc6-4a79-9089-fdcfee39702d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/role_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e67a7c48-0964-4473-b63d-056ec4fab6b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/25 SETTING PERMISSION/STPM01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afdbaec4-93ad-4c38-ae49-e0976511bb4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/25 SETTING PERMISSION/STPM01TC02 -- Success_Permission-List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14bb6d05-97bb-4aed-8dbb-0509d5f0501e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/25 SETTING PERMISSION/STPM02TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
