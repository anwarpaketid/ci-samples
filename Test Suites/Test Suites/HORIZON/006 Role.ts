<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>006 Role</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cee6a4d2-0b70-474e-980d-815ffd81720a</testSuiteGuid>
   <testCaseLink>
      <guid>1399a013-08e5-4ae2-9148-46f9d16c1316</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a10cf157-07a8-4f47-99eb-37af42746bac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bd06453-8e21-4fc3-ac7e-e47158e5e7a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06 SETTING_ROLE/RLE01_GETROLE/RLE01TC01_GETROLE_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c0077f5-9c29-4315-ad69-e0d289845647</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06 SETTING_ROLE/RLE02_CREATEROLE/RLE02TC01_CREATE_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>065490bc-3b86-40d9-bc50-681a5680d319</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/06 SETTING_ROLE/RLE03_UPDATEROLE/RLE03TC01_UPDATEROLE_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee0ba0b3-ad60-494b-b905-1b86bea7825f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06 SETTING_ROLE/RLE05_ROLELIST/RLE05TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74431964-976c-4cf2-adef-4a0af92f0a4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06 SETTING_ROLE/RLE04_DELETEROLE/RLE04TC01_DELETEROLE_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
