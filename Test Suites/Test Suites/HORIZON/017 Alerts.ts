<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>017 Alerts</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1096af2c-3812-4aac-b858-81e6bbc3ff19</testSuiteGuid>
   <testCaseLink>
      <guid>ecd05a20-8eae-4e51-b218-56a886372e3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a887eaad-3837-41e1-954c-3780e3778aaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>308e15f9-3b20-4b6e-92a5-bf5b73833958</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/getdate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4bf63d85-a6ea-4017-a5a3-94b35b404173</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/17 ALERTS/ALRT01 -- POST/ALRT01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>672720d0-1863-4ece-8f72-c88c738ee4c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/17 ALERTS/ALRT02 -- GET/ALRT02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92306df6-05c5-45c1-8a7d-f6675b30676d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/alrtid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f7129ae-e942-49e0-a374-281f0b904d73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/17 ALERTS/ALRT02 -- GET/ALRT02TC02 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39b34a67-2e38-4d0f-853b-ee1f12095ac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/17 ALERTS/ALRT03 -- PUT/ALRT03TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
