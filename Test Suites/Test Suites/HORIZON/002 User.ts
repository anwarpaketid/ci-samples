<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>002 User</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f10a3ecb-9993-492a-a95d-75ae25551f0b</testSuiteGuid>
   <testCaseLink>
      <guid>2d645971-a224-4595-b0e8-896522756777</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec26bc83-a998-4f25-b7ea-4d3ba73e69d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5913c51-4f7b-4053-8743-7dd82ca04222</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4701285f-d52f-46d3-aa46-3260ef18eb9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/GRP_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8a9e4be-3fa2-4542-a337-0e2c0d48e31a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/orgid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1214e229-a350-4a53-b909-2ef3a1263765</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/role_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf66843d-2d8f-4879-b383-70c5a18af355</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR01_GETUSER/USR01TC01_success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4b8f6da-bdcb-456d-93e0-e2d953d40522</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC01_success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d14f5503-b6ea-4860-89df-f5904227f837</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/user_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a00e3701-7880-4b47-82d1-9405115624e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR04_PUTUSER/USR01TC01_success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a7ac8fb-0eda-4260-b450-8f5b0953bfb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR05_GENERATE/USR05TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa7bdc72-1031-4c42-9366-54c439f421f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR06_USERLIST/USR06TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb159da5-ea5f-40d2-88e9-98c02cb9bb52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR07_OBJLIST/USR07TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
