<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>029 Webhook</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a7bcf9f0-14a6-4970-bdf4-69805e866ba6</testSuiteGuid>
   <testCaseLink>
      <guid>aa0a4e7c-5e31-4b19-a857-733fe6b1673f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56959e51-85e7-4158-9c13-c0db25e77291</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>768c2476-47e9-4fc7-8fc5-06372ec4dc64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/29 WEBHOOK/WHK01TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff58c42a-9fb3-4385-8827-1c732b60cc08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/29 WEBHOOK/WHK02TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65567cb3-4695-444f-9a6e-481486746797</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/whook_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df8e5c5c-9607-4a44-99eb-8b750a3d29aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/29 WEBHOOK/WHK03TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>405e2847-124d-4541-8548-4d33d220dabd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/29 WEBHOOK/WHK04TC01_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
