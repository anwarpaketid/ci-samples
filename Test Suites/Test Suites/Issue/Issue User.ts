<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Issue User</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>40bd24e7-adeb-4ede-9bd8-7f395a0f2006</testSuiteGuid>
   <testCaseLink>
      <guid>2d645971-a224-4595-b0e8-896522756777</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5913c51-4f7b-4053-8743-7dd82ca04222</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4701285f-d52f-46d3-aa46-3260ef18eb9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/GRP_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8a9e4be-3fa2-4542-a337-0e2c0d48e31a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/orgid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1214e229-a350-4a53-b909-2ef3a1263765</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/role_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4b8f6da-bdcb-456d-93e0-e2d953d40522</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC01_success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a93dc7b0-8abf-4598-b22c-9d90a5c482bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC03_allNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcbf5ac8-b4a7-48bb-bf8b-0972d79ebd66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC04_roleNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c21f310e-9bf3-477f-ac43-534bbca06763</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC05_nameNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d7a26ae-e76b-489a-a4cc-78de1c7b6e12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC06_usernameNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cba10712-fc40-41ff-b2d9-2833c351f8eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC07_emailNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ecdcb53-d282-4ce7-95cd-e5c10ff4d0a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC08_passwordNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8ae07ab-65d7-4a58-9ae0-dbc86f5563a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC09_phoneNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae2779e8-ba3a-4863-bae5-91bf85010156</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC18_groupNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1065d56d-6136-45ba-9b3b-a6373c7a83d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 SETTING_USER/USR02_CREATEUSER/USR02TC21_locationNull</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
