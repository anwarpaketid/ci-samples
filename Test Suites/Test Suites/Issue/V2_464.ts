<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>V2_464</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>12630884-7d33-4ee8-b590-218caae4ade5</testSuiteGuid>
   <testCaseLink>
      <guid>46c3a42a-0e68-45ed-8b50-3f852525342b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/000 - API KEY</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15caa244-280b-4ed1-a886-9b196f671262</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/000 - LocationV2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b440dc5-3668-4b4c-a44a-7a62dde3ba26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN07 - PUT Shipment by Booking Code/001 Positive Picked</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>837bf1d7-47e7-4ea3-9f1f-7ea2a4ce9cc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN07 - PUT Shipment by Booking Code/002 Positive Inbound</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82f9dc15-2a36-44f7-820c-a465c0ac0370</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN07 - PUT Shipment by Booking Code/003 Positive Delivered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e11f248-31fc-4bec-adc7-f23835551f0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN07 - PUT Shipment by Booking Code/004 Negative BookCode Not Found</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea68260c-9c99-4593-8364-3681951c4544</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN07 - PUT Shipment by Booking Code/005 Negative BookCode Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be7aaa8c-e1ba-45b0-b08d-089f0fb9a7b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN07 - PUT Shipment by Booking Code/006 Negative Status Null</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
