<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>027 Task Field</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>247c1c78-c7fe-4a3a-bf54-a61c139692ce</testSuiteGuid>
   <testCaseLink>
      <guid>64ce5c9e-14d6-4a36-8231-0c13d9500d62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53275c4e-69d6-4a30-8739-f3649da6d8bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tsfdid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>258c4d2d-46d8-4168-90e1-fef12646245e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TSFD01TC02 -- SuccessById</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32b6a2a2-1045-462c-80f3-9194f1bf8d49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TSFD01TC03 -- SuccessAllFlowName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd31784e-d239-4c08-afad-c7c63990ec5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TSFD01TC04 -- Success All Task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f3502d4-4159-4001-837d-da1a8872e4a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TSFD02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3d92efe-aacf-487d-8760-c3611b14c2dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TSFD03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c19dc60-4a8b-42c5-863c-0a4a12bdef59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TSFD04TC01 -- Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>09972692-f120-4d06-a901-154f8a4c413e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8b7e9b95-966c-448d-b714-021786bdae45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TASK TYPE/TSTY01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ea886bc-886f-46ce-a377-15d9a102092a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e924d10c-13eb-4846-8a2c-c8f980e1206e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TASK TYPE/TSTY01TC02 -- SuccessById</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1d7189e-9c03-470b-87a0-e3683e18cdb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/27 TASK FIELD/TASK TYPE/TSTY02TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
