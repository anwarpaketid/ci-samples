<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>V2_473</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ff43f021-5bb5-44df-b1a8-ab2fe59ee6c3</testSuiteGuid>
   <testCaseLink>
      <guid>2faf58ed-8bcf-49f7-be5e-2daaac46ec20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/001POSITIVE - all param</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f6588b5-650a-48cd-a7c4-6f1d2ce5a0f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/002POSITIVE - VendorId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18e7aa4b-6305-474e-b193-9a88f755d344</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/003POSITIVE - from zipcode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a433c7c-f06d-48ab-9bfa-4a821139e8a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/004POSITIVE - to zipcode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc9c70bf-4ebf-4de5-aa6f-7b3cbb03b62f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/005POSITIVE - fromZipcode null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc184673-7ae5-41ec-b6c9-dd00f3601b08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/006POSITIVE - toZipcode null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>841bbae9-a99a-4418-a8cd-208574bddee1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/007POSITIVE - vendorId null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49bc2dec-5f71-44ad-8fa4-498dbaec0eb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/001NEGATIIVE - tozipcode not exist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b77e11fe-1343-4c94-8757-d5bd0c320fd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/002NEGATIIVE - fromzipcode not exist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14156713-eee0-49ea-ba16-3dac714413a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN08 - GET Tariff/003NEGATIIVE - vendor_id not found</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
