<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>008 Organization Data</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9a69af1d-8429-462b-b13f-1f5419f942cb</testSuiteGuid>
   <testCaseLink>
      <guid>db9306e6-0bfa-475d-bc26-1b16c69fe764</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b659d1d-5fc6-4832-9e2e-2a46b8c50160</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcaeebbe-d5f8-4afb-af62-9e4f2e08d4d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08 ORGANIZATION DATA/ORGD01_READ/ORGD01TC01_READ_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>030752f7-a847-4a4a-877e-677462b37fdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08 ORGANIZATION DATA/ORGD02_CREATE/ORGD02TC01_CREATE_SuccessArray</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca574bf0-9e14-4af5-a442-acae03b6ac38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/orgid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3f808a6-81d6-4dd0-93b1-b82b37c73621</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08 ORGANIZATION DATA/ORGD03_UPDATE/ORGD03TC01_UPDATE_SuccessArray</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60bd8b44-94eb-4bfe-9396-a2c66572e319</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08 ORGANIZATION DATA/ORGD05_SHOW/ORGD05TC01_SHOW_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbfa25f7-c456-4c1a-9395-1376c21c3172</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08 ORGANIZATION DATA/ORGD04_DELETE/ORGD04TC01_DELETE_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c7c5ca1-0a6e-41d7-9cc5-d8f623dda6e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/orgname</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f53534d-872b-4099-86d1-7a8823fc87a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/08 ORGANIZATION DATA/ORGD06_SHOWBYNAME/ORGD06TC01_SHOWBYNAME_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
