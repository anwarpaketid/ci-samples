<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ISSUE RACK</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>43ae7c74-8846-44d3-ba9f-b237cb24a55c</testSuiteGuid>
   <testCaseLink>
      <guid>83ac1ca6-4c8f-48cb-8e1b-04f811335c50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>598e72ce-56df-40db-b116-1b0e4ac61e25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>865fbf00-3e74-4a9e-a0a6-429d7cb36ff6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue RACK/RCK01 - GET SUCCESS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31bd6e22-bde3-4392-9a13-4bd1fc39ac3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue RACK/RCK02 - POST SUCCESS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e64dace3-f604-4a7e-9004-d5f6bdd59e7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue RACK/RCK03 - PUT SUCCESS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8553529f-8ade-4e37-b6ab-fcf7c765f48d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue RACK/RCK04 - DELETE SUCCESS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54c050b2-41bf-4cf8-b072-7c5892c2fafc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Issue RACK/RCK01 - GET SUCCESS</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
