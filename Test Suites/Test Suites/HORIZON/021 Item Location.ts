<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>021 Item Location</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a8771737-2e44-4287-8e7c-5833251c62c0</testSuiteGuid>
   <testCaseLink>
      <guid>bfd6667c-ec0d-4f47-ad3c-0779283f015a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24e21be6-c882-4e35-abca-ffa465b3cb45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/21 ITEM LOCATION/ILOC01 -- GET/ILOC01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdce3346-b14d-4768-a735-25aa72c8ffe4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/21 ITEM LOCATION/ILOC02 -- POST/ILOC02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc459d70-d4d6-4271-b423-4bee7e6c6932</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/ilocid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76d73a1b-6820-4c4d-b297-0ed2fcbe2dfe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/21 ITEM LOCATION/ILOC03 -- PUT/ILOC03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38596d6e-bcb9-4a97-aa36-6c8efe628906</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/21 ITEM LOCATION/ILOC04 -- DELETE/ILOC04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
