<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>028 Task Filter</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8321dbce-ec35-4413-9e40-f94f5e15ba0d</testSuiteGuid>
   <testCaseLink>
      <guid>2b74804a-e33b-47a2-b042-13376dce9a01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46cfaa82-f1d3-479c-8042-3704ee6d44a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/28 TASK FILTER/TSFL01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0d80619-c5f3-4f5e-abed-984ac35cbd4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/28 TASK FILTER/TSFL02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a725b46e-11f7-4350-a0b1-2099f58670e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/28 TASK FILTER/TSFL03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6649efe-976f-4640-8178-4dc4fc84d92b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/28 TASK FILTER/TSFL04TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a164512-8996-4d34-a6fd-caf74bb06b62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/28 TASK FILTER/TSFL05TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad712f3d-980f-4043-94d2-445eafb80229</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tsflid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2a85aa5-bc08-40cf-b830-9e375a02fae0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/28 TASK FILTER/TSFL06TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
