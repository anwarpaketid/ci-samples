<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>010 Cost</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0d9a9f61-82e6-4700-a35c-afbfa882aa7f</testSuiteGuid>
   <testCaseLink>
      <guid>0ba787f5-781e-46cd-ada1-d397b2ed663c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd537659-5461-469c-a3ef-6a16af81d2be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>678760e1-7b7d-4d8b-bebd-51ae8a8c77e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10 COST/CST01 -- GET/CST01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0dca4208-f1b9-45ab-96fa-20c309354443</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10 COST/CST02 -- CREATE/CST02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>989b9618-aa71-4113-aacd-ead44151c72a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/cstid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb43e2df-84b0-4f97-bae7-ffc7c4af6906</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10 COST/CST03 -- PUT/CST03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efef17a5-1b5c-42d9-9d33-0b83cece5084</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10 COST/CST04 -- DELETE/CST04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
