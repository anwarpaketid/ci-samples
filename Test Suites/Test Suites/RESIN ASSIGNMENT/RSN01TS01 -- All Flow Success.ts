<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RSN01TS01 -- All Flow Success</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b63fd7b2-0163-4f0e-b903-d436d68cffb5</testSuiteGuid>
   <testCaseLink>
      <guid>d69595af-fd4b-4db9-be42-1c3125327a54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN02 -- Assignment/RSN02TC00 -- Preconditions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>842bffe2-ea2a-460e-8704-9b41b115b1d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN02 -- Assignment/RSN02TC01 -- Success POST</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d28fd41-a93b-47c8-9343-956f9384d094</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN02 -- Assignment/RSN02TC02 -- Success PUT</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
