<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>038 Auto Assign</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3198a889-484d-4ae2-b426-069e75d8b5af</testSuiteGuid>
   <testCaseLink>
      <guid>e261d2b3-c6af-4003-b960-94306264ce42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8c0ae5e-58d5-4d12-8abe-5d0b0617766a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b127005e-5f33-4e4f-ac62-a3547ec63b1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28c19ad4-4d24-426a-9734-454ba02e0ad2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/38 AUTO ASSIGN/AUAS02 -- POST/AUAS02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b308aff-c11c-4ac6-84fe-88b2774e9454</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/38 AUTO ASSIGN/AUAS01 -- GET/AUAS01TC01 -- GET All</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffa59a92-ff6d-48dd-84d5-b75b88eeedd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/auasId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87d8f4a7-af08-4cd3-9a3b-570b93f78526</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/38 AUTO ASSIGN/AUAS04 -- DELETE/AUAS04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
