<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>V2_467</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>da6be9c8-4529-424a-9f27-d81bcadc0843</testSuiteGuid>
   <testCaseLink>
      <guid>a7459bd0-26ae-4264-9eb4-9c3f25efb33a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/FLOW/000 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d46607f-1324-4011-b4a4-dd8ca450fce4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/001 Post Shipment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a87009a3-aea6-4cef-b870-0187792a88fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/002 GET Shipment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2beee38a-afeb-4245-b16d-a8e82257c4e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/003 Post Pickup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d25d9d0e-d322-4d6a-8afa-acd5466c7aa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/004 GET Pickup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc0ea528-bac1-4b9e-902f-8a8183abf0b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/005 Post Inventory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75890e4c-9417-419c-9c3b-bd0031d86dff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/006 Negative Post Shipment Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>914156e9-a639-4748-bd6f-86426b72135f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/007 Negative Post Pickup Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7acfe169-36ca-4231-8dee-152d020fe1e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN09 - LocationCode/008 Negative Post Inventory Null</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
