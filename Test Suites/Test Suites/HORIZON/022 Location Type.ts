<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>022 Location Type</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>414bd1ea-d59e-47fd-8c83-1724761fb2ec</testSuiteGuid>
   <testCaseLink>
      <guid>8f3f948b-b6bc-42d7-be5f-c6ff2f2cb395</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb87fb3c-c9ad-4a63-9d74-4c897a542503</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/22 LOCATION TYPE/LCTP01TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38fd81bd-50b3-4cf6-af9f-e9bb7a872981</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/22 LOCATION TYPE/LCTP02TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>138adc01-fdb0-4e9d-8578-81db2404f2f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/lctp_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60f8f7e2-0c7d-4a00-85b7-61e74a2e7bb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/22 LOCATION TYPE/LCTP03TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c17b769d-9531-449d-85d7-57a27c5781eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/22 LOCATION TYPE/LCTP04TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89154827-7a64-49b9-8f5d-7125c1640fce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/22 LOCATION TYPE/LCTP05TC01_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
