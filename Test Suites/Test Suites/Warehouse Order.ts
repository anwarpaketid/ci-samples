<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Warehouse Order</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d8a58e9e-b07e-4ee1-8641-e787af4c07a3</testSuiteGuid>
   <testCaseLink>
      <guid>134ba9fa-6879-48c2-a656-2c162a15e7bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>863421b2-0bab-4f4a-bd84-41911ea68972</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>895efdf3-dd37-4062-8846-40927d341c6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/11 WMS/ORDER/ORD01 -- POST INBOUND/ORD01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d13e4711-5ca9-479f-b1fa-2049f220dbca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11 WMS/ORDER/ORD02 -- POST OUTBOUND/ORD02TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
