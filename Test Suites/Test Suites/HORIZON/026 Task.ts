<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>026 Task</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a0642479-0352-43e6-92da-3bb6f7e2138c</testSuiteGuid>
   <testCaseLink>
      <guid>74fa0196-4327-47e8-a2b1-67987445331b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48807602-139d-4609-b9cc-6d017c412a4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/taskid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d455132-2bc6-48cb-9f3d-1f982164d7b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/26 TASK/TSK01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f15ed83-489e-4c13-89cc-880be9a9f9b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/26 TASK/TSK02TC01 -- GET Task Column</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2db4e75-17c6-4741-ab63-2d302e607ac5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/26 TASK/TSK02TC02 -- GET Task Column Elastics</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcc174a4-56c8-41c4-8649-92968a705e0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/26 TASK/TSK02TC03 -- GET Task Column Firestore</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bef32bb6-c941-46ed-bab4-e5414f64381d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/26 TASK/TSK02TC04 -- GET Task</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
