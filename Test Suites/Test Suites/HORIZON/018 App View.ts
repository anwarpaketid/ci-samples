<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>018 App View</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>251f738b-8abb-42ee-8b52-8d24f0ebe4ba</testSuiteGuid>
   <testCaseLink>
      <guid>475c9ae3-b309-417c-9192-bbee78904b3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1927d077-4007-40c9-a836-190a4b7cd0f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18 APP VIEW/APPV01TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb737984-314f-4481-a658-2f89cd3b225d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18 APP VIEW/APPV02TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56ee96ad-f83e-42ea-b3c4-d9064a7dbd72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/appviewId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40a3669d-a13b-4c20-b1a1-c8ac614c7a93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18 APP VIEW/APPV03TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>719f87b7-bdd4-4193-9a3d-4fc2d6cfde80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18 APP VIEW/APPV04TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c2da855-3b94-4c68-ba79-578021e94140</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/18 APP VIEW/APPV05TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd74a41e-54ee-4adf-aa0b-67b6e8b3b025</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18 APP VIEW/APPV06TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4198418-380e-473a-83f0-dd0d7ac84c6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/18 APP VIEW/APPV07TC01_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
