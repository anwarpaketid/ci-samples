<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>V2_462</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c6764fef-4960-4d21-b39d-58242bbdd254</testSuiteGuid>
   <testCaseLink>
      <guid>54bd89f0-112e-4ab7-a500-c501f94ca8ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/FLOW/000 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>811a489e-79eb-47ce-b95d-635f5b631892</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/000 - LocationV2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3c3a6db-ba5e-4617-b427-ab5e32a080b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/001 - Hub</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2edb76d6-ea6c-4956-a38f-d4b775bdf080</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/000 - LocationMile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d43512ee-597d-4461-893a-056c2f37209d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/002 - Kurir</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2065774-55b8-4826-9ac4-878333a91081</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/003 - Type Not Found</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5505a0c0-c1e6-43c0-be5d-6062bb5f48e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/004 - Type Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32e16293-6c87-4413-8a4b-a6840cd21bfd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/005 - LocationId Not Found</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bde2269b-99eb-4193-85fd-47502f9608b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/007 LocationName Null</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cb263e9-6c08-4bcd-917e-67a395147b7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN04/006 - LocationId Null</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
