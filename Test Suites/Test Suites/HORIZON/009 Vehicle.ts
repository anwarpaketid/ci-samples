<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>009 Vehicle</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c268cd40-63f7-4393-8a02-0aa85b71d510</testSuiteGuid>
   <testCaseLink>
      <guid>08cc3858-270e-42fd-af86-31054129ebd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29d6f704-932d-41fe-b154-8f4458179ec9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5faee60c-de52-4d61-a817-fe637e24a835</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/user_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dda1149f-e908-42e9-8ccf-9ad9d2152967</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/vhtype_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e83280c-c674-4768-8521-adaf60a67cfa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eced51ac-1b84-4bf5-b40d-1b03c54f4ab1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09 VEHICLE/VHC01 GET Vehicle/VHC01TC01--GET--SuccessAllLoc</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0efa201c-62c5-4110-86c1-3459bc259ae9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09 VEHICLE/VHC02 POST Vehicle/VHC02TC01--POST--Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef6838b7-92af-4512-88d0-bfb1c4cd0fdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/vhc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85a49623-cf89-42f2-9d1a-eb0f93030435</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09 VEHICLE/VHC03 UPDATE Vehicle/VHC03TC01--UPDATE--Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6be82abd-8ec0-4a5d-89fb-93a241d9dc6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09 VEHICLE/VHC04 DELETE Vehicle/VHC04TC01--DELETE--Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
