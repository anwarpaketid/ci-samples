<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Issue 1140</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>92620d72-25c7-4981-8b12-bfee09f21fb9</testSuiteGuid>
   <testCaseLink>
      <guid>7c1d5e22-04d9-464f-8a33-bfd9a71b0fc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21ee213e-55d8-4e2e-badb-375f779ae400</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02d8ca85-39be-473b-8c4d-6407cbfb9e16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81b7862d-19f3-4821-a93d-05165a480ff2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Positive/TCPOSITIVE001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9edf20d-5ebe-44e2-8a2e-8a5b7cea577d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Negative/TCNEGATIVE001</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
