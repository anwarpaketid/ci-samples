<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>V2_476</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3d21c026-b654-4542-a3d4-2411808df518</testSuiteGuid>
   <testCaseLink>
      <guid>10d830ad-077e-4cd6-b711-bbd44f29ac29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/FLOW/000 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15134525-f378-42a2-b232-d56e03e91e98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/001 PositiveDefaultTrue</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5fbf35a-1553-405f-b36e-73002b66eae5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/002 PositiveDefaultFalse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a762b3a-d2f5-4bdf-bc31-cbbb3b1f69c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/003 GETPickup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83946cae-de84-409f-8774-647e0a7a9a45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/000 GetLocationPickupId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce1828b2-dd47-41e7-8377-33398fb6047e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/005 PutToTrue</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a55964e-6ca4-4331-9a4c-f3e6782be7fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/004 PutToFalse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a5ced46-edfc-4977-bc27-568e828a06d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/006 PostDefaultNull</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4edecf0-9571-4dd4-b130-1fc2d0bc7625</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN03 - Location/002 POST Location/ISSUE 476/007 PutDefaultNull</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
