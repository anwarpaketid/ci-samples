<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>V2_472</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8a2005cf-def6-4409-bb2c-1b01bd8aecce</testSuiteGuid>
   <testCaseLink>
      <guid>8a3dbbbd-d1d7-412e-9268-fcfaf4699801</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN10 -- Service Direct Tariff/001 - Positive</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>752e3857-7b95-43bd-af48-6b8522a48bd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN10 -- Service Direct Tariff/002 - ServiceNotDirect</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>086eb615-677d-44ad-852b-aceb5a1d8683</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN10 -- Service Direct Tariff/003 - ServiceEmpty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a53819f1-e3d9-4268-8d9d-42f06dab1eb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/48 RESIN/RSN10 -- Service Direct Tariff/004 - noBodyService</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
