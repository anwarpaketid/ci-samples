<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>015 Zone Group</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c1b8c152-38a6-46cd-8436-4bcc7be30f7d</testSuiteGuid>
   <testCaseLink>
      <guid>8d0d58a6-8660-405c-af80-5b1aa775e332</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0999849-919a-4424-9872-fc29ecde45f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e82ad604-27c9-4330-9e87-8f4fd7496673</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e81e6190-1375-4e39-8ff3-6db2eb7387fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/15 ZONE GROUP/ZG02 -- POST/ZG02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be76c4e2-e97a-4ab1-843a-f70c38643515</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/15 ZONE GROUP/ZG01 -- GET/ZG01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edccda58-decd-4afd-a1b0-db943ff57b35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/zgid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d778a46-f5c8-4959-ac07-17b90b459861</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/15 ZONE GROUP/ZG03 -- DELETE/ZG03TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
