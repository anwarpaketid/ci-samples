<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Vehicle Type</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f54c99ca-1a82-40da-8126-b26a3d9a2689</testSuiteGuid>
   <testCaseLink>
      <guid>8e3a081f-5f28-40cc-be26-62cb84c969c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b473a050-5fcb-4729-909f-1b109fc098a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b48a81f1-6402-4794-9390-a84938bf8a48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY01 -- GET/VHTY01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42b889cc-1dc5-4eac-a89e-58f9a01a96c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY02 -- POST/VHTY02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15fb3e37-8afe-462e-ae93-1833fd093d7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY03 -- PUT/VHTY03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fae6e8f9-1caa-4692-85b6-4b16d0be0450</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/36 VEHICLE TYPE/VHTY04 -- DELETE/VHTY04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
