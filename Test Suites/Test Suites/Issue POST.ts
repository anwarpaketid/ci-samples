<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Issue POST</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aa30734a-25ff-4b9a-959c-ddccd7297836</testSuiteGuid>
   <testCaseLink>
      <guid>f7ff1e12-ddad-4d50-afd2-cba6ea369c0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d86a826d-5610-42c2-a694-97d9e053c30b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>489bcd3e-f27f-43ca-b88c-0cbd59e71eef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f33d538-4933-4b90-a3e8-6752ad7bc5df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Positive/TCPOSITIVE001 - Format h</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4a41584-34a2-4603-818a-60b3871b16c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Positive/TCPOSITIVE002 - Format Datetime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8bd2d41-4195-4f6e-bb17-f2622065ad4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Positive/TCPOSITIVE003 - No SLATime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a822eeba-0be6-4ae0-840a-48e4b8ea712c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Positive/TCPOSITIVE004 - Format Datetime (kurang)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3335ffa-9df7-4647-a434-57005b867606</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Negative/TCNEGATIVE001 - FormatSLAInvalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>252e2650-3f9a-4186-a4c2-b33794726f6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Negative/TCNEGATIVE002 - StringKosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7460d192-c738-4b79-b44b-d66da3d11aef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 1140/Negative/TCNEGATIVE003 - Format jam minus</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
