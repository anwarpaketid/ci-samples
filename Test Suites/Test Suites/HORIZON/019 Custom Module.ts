<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>019 Custom Module</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8d3af35e-1560-44d3-bfe9-adb711ce5d4f</testSuiteGuid>
   <testCaseLink>
      <guid>0bff25ad-cc94-4dcd-9b75-edece39875a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87988ef7-ad81-45de-81a3-5390005dd0ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/19 CUSTOM MODULE/CMDL01TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c43d087-fe79-4c71-b4c7-b7e7dd76f331</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/19 CUSTOM MODULE/CMDL02TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>036a576a-1376-4dda-998b-f8192d5b0908</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/cmdl_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eeaf7c7e-034e-41d3-a5db-6936d355a38e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/19 CUSTOM MODULE/CMDL03TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c577c09-a427-4f03-8f12-3f55dd73bee7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/19 CUSTOM MODULE/CMDL04TC01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59ee22eb-3a4f-42d8-8a62-2f5bbdd907ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/cmdl_name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c86a19f1-353d-40ef-ad6c-3966960fe91c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/19 CUSTOM MODULE/CMDL05TC01_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
