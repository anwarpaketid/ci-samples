<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>007 Organization</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5ceff138-57ae-4ec0-b56a-64d6cfac6293</testSuiteGuid>
   <testCaseLink>
      <guid>c620a109-10ff-4054-8309-bf7540eb9347</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07 SETTING_ORGANIZATION/SORG01_CHGPASS/SORG01TC01_CHGPASS_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6df394fc-fff4-4455-9c0a-5ecf5bf279a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07 SETTING_ORGANIZATION/SORG02_INFO/SORG02TC01_INFO_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ea31432-80a1-4856-9e9c-78e70c1bfffc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07 SETTING_ORGANIZATION/SORG03_UPDATE/SORG03TC01_UPDATE_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c986cbb6-6935-4f1c-a724-d414f2692660</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07 SETTING_ORGANIZATION/SORG04_GENKEY/SORG04TC01_GENKEY_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
