<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>033 Request Demo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fbcbd04f-3bcb-4523-863f-72591ee41650</testSuiteGuid>
   <testCaseLink>
      <guid>37b59d62-086f-4aed-9fa1-e5c968446474</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ad0cbf0-0bbc-4df8-975e-37d17a3edede</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/reqid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>536363a2-4ca0-4ea0-8202-9d219deaa944</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ebb1c79-abc3-45b5-8166-75badc97ba40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/33 REQUEST DEMO/REQ01 -- GET/REQ01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb054591-87e8-469e-b457-a9a883d4db93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/33 REQUEST DEMO/REQ02 -- POST/REQ02TC01 -- Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d642421f-5151-4fe9-8c99-43814856a5b3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f8225fff-d5c5-4c30-b5fd-3e178d550d26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/33 REQUEST DEMO/REQ03 -- PUT/REQ03TC01 -- Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e63463b7-a336-40a9-9eca-14c36549cc7b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f7bff7eb-6837-4a85-a743-b5c34a7e646f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/33 REQUEST DEMO/REQ04 -- DELETE/REQ04TC01 -- Success</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>faeec235-f159-4b68-b46b-b619694120a5</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
