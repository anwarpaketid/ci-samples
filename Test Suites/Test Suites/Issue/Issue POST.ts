<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Issue POST</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aa30734a-25ff-4b9a-959c-ddccd7297836</testSuiteGuid>
   <testCaseLink>
      <guid>f7ff1e12-ddad-4d50-afd2-cba6ea369c0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d86a826d-5610-42c2-a694-97d9e053c30b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>489bcd3e-f27f-43ca-b88c-0cbd59e71eef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c0856ef-7ee4-4f76-baa3-ebe4fa1f67e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Positive/TCPOSITIVE001 - Format h</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>556b33a4-4198-4eff-ad23-0fa9140426f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Positive/TCPOSITIVE002 - Format Datetime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7696e1f5-eea1-4fa8-a790-5b460dd5f913</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Positive/TCPOSITIVE003 - Format Datetime (kurang)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08a29777-19a0-4fe5-89cf-72ba8a3a1403</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Negative/TCNEGATIVE004 - No SLATime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f02eb60-6d4b-45f5-b606-a46db8c68407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Negative/TCNEGATIVE001 - FormatSLAInvalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a110d21-d23f-4eef-aad1-cec6393c5abf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Negative/TCNEGATIVE002 - StringKosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>025d968f-eac3-4a39-89cc-165af2129163</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Negative/TCNEGATIVE003 - Format jam minus</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
