<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>013 Saldo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ab031997-d51b-4ddc-a878-12cadedee268</testSuiteGuid>
   <testCaseLink>
      <guid>c037aeab-4fd5-4c13-ac60-b53f79c02246</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe550bc2-b2ee-4976-bbdf-08fd98e58494</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbc383cc-2058-432f-ab63-120f23f97552</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/randomizer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>420ff267-0f6d-482c-9d6f-21f157f9484d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/13 SALDO/SLD01 -- POST/SLD01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d01e0cc2-31aa-41fe-88fa-91944de570ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/13 SALDO/SLD02 -- GET History/SLD02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>270673db-dec8-48e9-bc3e-14a560ab2de1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/13 SALDO/SLD03 -- GET Balance/SLD03TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
