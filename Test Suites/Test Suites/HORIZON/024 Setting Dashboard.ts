<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>024 Setting Dashboard</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>58d1ebf7-c05f-4ccf-8cc3-c863e43ad44d</testSuiteGuid>
   <testCaseLink>
      <guid>3c196d20-6c23-4ff7-8034-3b9159857761</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aab53c2c-fdc3-41f2-9144-a06b5419ebc8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cf41d3e-2751-450a-b1d7-566264cf3726</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/stdbid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0ea5c90-0333-4af6-91f9-9ad624b0464f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB01TC02 -- Success with ID</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c2ab9cb-eac9-449e-bdf0-cd03c16275f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB01TC03 -- Success with Field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fbb3f64-e119-40cd-ab3e-4cb94d71b5e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB01TC04 -- Success Value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d24aed34-13fa-45e9-822d-ced5b04ce5fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/role_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fecfe05e-aed4-4740-89f6-df2c22a914f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1c18c17-b747-4d0f-874e-d02f7b6ea5a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61234221-6257-4ec3-bcce-e2a9d3521bb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/24 SETTING DASHBOARD/STDB04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
