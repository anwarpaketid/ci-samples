<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>035 Web</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c93ed51b-a8fb-433c-a0e9-06cf686375bf</testSuiteGuid>
   <testCaseLink>
      <guid>03a1fd94-fe9b-4d70-8c6e-49544f8992dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a46e2603-568b-4fdd-9688-17df430a13e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/35 WEB/WEB01 -- Get Web/WEB01TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>425e8576-e472-44c3-ad34-b3ec5e91e0ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/35 WEB/WEB02 -- Get WebDeveloper/WEB02TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59edf456-b638-4726-adcd-a9db895c584f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/35 WEB/WEB03 -- GET Mail Register/WEB03TC01 -- Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21b0e76a-74b2-44f0-920e-36894c5dd1b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/35 WEB/WEB04 -- GET Report Org/WEB04TC01 -- Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
