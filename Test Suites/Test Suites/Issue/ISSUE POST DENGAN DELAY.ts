<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ISSUE POST DENGAN DELAY</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9370af72-6c9e-4a76-b486-b697c3dfde3d</testSuiteGuid>
   <testCaseLink>
      <guid>0569333c-7b43-4559-bd38-3e269dc5e0bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/X-Auth-Key</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89f408b4-1faf-4759-8f91-0caf2540c2d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/tasktypeid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45c2043a-8b04-4607-85bf-a2702f439dd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Preconditions/loc_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34ef349f-f62f-4ff1-b53d-38c3f95f16bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 973/Positive/TCPOSITIVE002 - Format Datetime</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c49e155-ed19-4671-b8f7-a96e385da092</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCPOSITIVE001 - PUT Task Item Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd147c71-d38d-4e84-9435-1769f2ce596d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCPOSITIVE002 - PUT Edit Task Item</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>238291d3-0ef6-4d45-8e67-73f07e4beec8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCPOSITIVE003 - PUT Delete Task Item</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37194904-8bf9-4dc5-a6e0-0308dfb2dfe4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCNEGATIVE001 - PUT Task Item Baru QtyStringkosong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cbb300a-f270-4ef1-9bf8-6efa6ce2e930</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ISSUE RELATED/Mile 969/TCNEGATIVE002 - PUT Delete tanpa mengosongkan UserVar</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
